package tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Delete {
    
    public static Boolean deleteSingleKey(final String col, final String table, final String id) {
        try {
            Connect.connect();
            Connection db = Connect.getConnection();
            
            String query = "DELETE FROM " + table + " WHERE " + col + " = ?";
            PreparedStatement delete = db.prepareStatement(query); 
            delete.setString(1,id); 
            
            int affectedRows = delete.executeUpdate();
            Connect.disconnect();
            return affectedRows > 0;
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
        return false;
    }
    
    public static Boolean deleteMultipleKeys(final String col1, final String col2, final String table, 
            final String id1, final String id2) {
        try {
            Connect.connect();
            Connection db = Connect.getConnection();
            
            String query = "DELETE FROM " + table + " WHERE " + col1 + " = ? AND " + col2 + " = ?";
            PreparedStatement delete = db.prepareStatement(query); 
            delete.setString(1,id1); 
            delete.setString(1,id2); 
            
            int affectedRows = delete.executeUpdate();
            Connect.disconnect();
            return affectedRows > 0;
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
        return false;
    }

}
