package tools;

import java.sql.*;

public class Connect {
    
    private static Connection connection;
    
    public static void connect() throws SQLException, ClassNotFoundException {
        Class.forName("org.mariadb.jdbc.Driver");

        connection = DriverManager.getConnection
          ("jdbc:mysql://localhost/medibase", "medibase", "medibase");
        
    }
    
    public static void disconnect() throws SQLException {
        connection.close();
    }
    
    public static Connection getConnection() {
        return connection;
    }
}
