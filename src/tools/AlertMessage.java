package tools;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class AlertMessage {

    public static void alert() {
        final Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Errore!");
        alert.setHeaderText("Operazione non consentita");
        alert.setContentText("C'è un errore nell'operazione richiesta, per questo non sarà eseguita. \n"
                + "Controllare i vincoli di sistema o contattare l'amministratore.");
        alert.show();
    }
}
