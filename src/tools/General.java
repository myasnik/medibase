package tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import medicines.ReferenceImpl;
import transactions.TransactionType;

public class General {

    public static Boolean isTransitionPossible(final String codMin, final String quantity, final TransactionType type) {
        try {
            Connect.connect();
            Connection db = Connect.getConnection();
            ResultSet rs;
            
            String query = "SELECT Quantita FROM REFERENZA WHERE CodiceMinisteriale = ?";
            PreparedStatement tot = db.prepareStatement(query); 
            tot.setString(1,codMin); 
            
            rs = tot.executeQuery();
            rs.next();
            int res = rs.getInt("Quantita");
            int val = -1;
            
            switch(type) {
            case VENDITA:
            case SMALTIMENTO:
            case RESO:
                val = res - Integer.parseInt(quantity);
                break;
            case ACQUISTO:
                val = res + Integer.parseInt(quantity);
                break;
            default:
                break;
            }
            Connect.disconnect();
            
            if(val >= 0) {
                updateRefQuantity(codMin, val);
                return true;
            }
        } catch (SQLException | ClassNotFoundException | NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    
    public static void updateRefQuantity(final String codMin, final int val) {
        try {
            Connect.connect();
            Connection db = Connect.getConnection();
            ResultSet rs;
            
            String query = "SELECT CodiceMinisteriale, QuantitaMinima, Quantita, "
                    + "NomeCommerciale, NomeFormato FROM REFERENZA WHERE CodiceMinisteriale = ?";
            PreparedStatement tot = db.prepareStatement(query); 
            tot.setString(1,codMin); 
            
            rs = tot.executeQuery();
            rs.next();
            ReferenceImpl ref = new ReferenceImpl(rs.getString("CodiceMinisteriale"), Integer.toString(rs.getInt("QuantitaMinima"))
                    , Integer.toString(rs.getInt("Quantita")), rs.getString("NomeCommerciale"), rs.getString("NomeFormato"));
            ref.setQuantity(Integer.toString(val));
            ref.update();
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
