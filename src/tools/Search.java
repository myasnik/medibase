package tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Search {
    
    public static Boolean isKeyPresent(final String col, final String table, final String id) {
        try {
            Connect.connect();
            Connection db = Connect.getConnection();
            ResultSet rs;
            
            String query = "SELECT " + col + " FROM " + table + " WHERE " + col + " = ?";
            PreparedStatement isPresent = db.prepareStatement(query); 
            isPresent.setString(1,id); 
            
            rs = isPresent.executeQuery();
            Connect.disconnect();
            return rs.next();
        } catch (SQLException | ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    
    public static Boolean areKeysPresent(final String col1, final String col2, final String table, 
            final String id1, final String id2) {
        try {
            Connect.connect();
            Connection db = Connect.getConnection();
            ResultSet rs;
            
            String query = "SELECT " + col1 + " " + col2 + " FROM " + table + " WHERE " + col1 + " = ? AND " + col2 + " = ?";
            PreparedStatement isPresent = db.prepareStatement(query); 
            isPresent.setString(1,id1); 
            isPresent.setString(2,id2); 
            
            rs = isPresent.executeQuery();
            Connect.disconnect();
            return rs.next();
        } catch (SQLException | ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

}
