package subscriptions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class RegistrationImpl implements Registration {
    
    private String subscriptionCode;
    private String buyerCode;
    
    public RegistrationImpl(final String subscriptionCode, final String buyerCode) {
        this.subscriptionCode = subscriptionCode;
        this.buyerCode = buyerCode;
    }

    @Override
    public String getSubscriptionCode() {
        return subscriptionCode;
    }

    @Override
    public void setSubscriptionCode(final String subscriptionCode) {
        this.subscriptionCode = subscriptionCode;
    }

    @Override
    public String getBuyerCode() {
        return buyerCode;
    }

    @Override
    public void setBuyerCode(final String buyerCode) {
        this.buyerCode = buyerCode;
    }

    @Override
    public void update() {
        try {          
            if(this.subscriptionCode.isEmpty() || this.buyerCode.isEmpty()) {
                throw new SQLException();
            }
            if(!isPresent()) {
                Connect.connect();
                Connection db = Connect.getConnection();
                PreparedStatement update;
                update = db.prepareStatement("INSERT INTO ISCRIZIONE(CodiceAbbonamento, CodiceAcquirente) VALUE(?,?)"); 
                update.setString(1,this.subscriptionCode); 
                update.setString(2,this.buyerCode); 
                update.executeUpdate();
                Connect.disconnect();
            }
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteMultipleKeys("CodiceAbbonamento", "CodiceAcquirente", 
                "ISCRIZIONE", this.subscriptionCode, this.buyerCode);
    }

    @Override
    public Boolean isPresent() {
        return Search.areKeysPresent("CodiceAbbonamento", "CodiceAcquirente", 
                "ISCRIZIONE", this.subscriptionCode, this.buyerCode);
    }

}
