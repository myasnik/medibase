package subscriptions;

public interface Subscription {

    public String getCode();

    public void setCode(final String code);

    public String getQuantity();

    public void setQuantity(final String quantity);

    public String getBasis();

    public void setBasis(final String basis);

    public String getStart();

    public void setStart(final String start);

    public String getEnd();

    public void setEnd(String end);

    public String getMinisterialCode();

    public void setMinisterialCode(final String ministerialCode);
    
    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();

}
