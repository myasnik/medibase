package subscriptions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class SubscriptionImpl implements Subscription {
    
    private String code;
    private String quantity;
    private String basis;
    private String start;
    private String end;
    private String ministerialCode;
    
    public SubscriptionImpl(final String code, final String quantity, final String basis, final String start, 
            final String end, final String ministerialCode) {
        this.code = code;
        this.quantity = quantity;
        this.basis = basis;
        this.start = start;
        this.end = end;
        this.ministerialCode = ministerialCode;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(final String code) {
        this.code = code;
    }

    @Override
    public String getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(final String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String getBasis() {
        return basis;
    }

    @Override
    public void setBasis(final String basis) {
        this.basis = basis;
    }

    @Override
    public String getStart() {
        return start;
    }

    @Override
    public void setStart(final String start) {
        this.start = start;
    }

    @Override
    public String getEnd() {
        return end;
    }

    @Override
    public void setEnd(final String end) {
        this.end = end;
    }

    @Override
    public String getMinisterialCode() {
        return ministerialCode;
    }

    @Override
    public void setMinisterialCode(final String ministerialCode) {
        this.ministerialCode = ministerialCode;
    }

    @Override
    public void update() {
        try {
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(this.basis.isEmpty() || this.code.isEmpty() || this.end.isEmpty() || this.ministerialCode.isEmpty()
                    || this.quantity.isEmpty() || this.start.isEmpty()) {
                throw new SQLException();
            }
            
            if(present) {
                update = db.prepareStatement("UPDATE ABBONAMENTO SET Quantita = ?, Cadenza = ?,"
                        + " DataInizio = ?, DataFine = ?, CodiceMinisteriale = ? WHERE CodiceAbbonamento = ?"); 
                update.setInt(1,Integer.parseInt(this.quantity));
                update.setString(2,this.basis);
                update.setString(3,this.start);
                update.setString(4,this.end);
                update.setString(5,this.ministerialCode); 
                update.setString(6,this.code); 
            }
            else {
                update = db.prepareStatement("INSERT INTO ABBONAMENTO(CodiceAbbonamento, Quantita,"
                        + " Cadenza, DataInizio, DataFine, CodiceMinisteriale) VALUE(?,?,?,?,?,?)");
                update.setString(1,this.code);
                update.setInt(2,Integer.parseInt(this.quantity));
                update.setString(3,this.basis);
                update.setString(4,this.start);
                update.setString(5,this.end);
                update.setString(6,this.ministerialCode); 
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException | NumberFormatException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("CodiceAbbonamento", "ABBONAMENTO", this.code);
    }

    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("CodiceAbbonamento", "ABBONAMENTO", this.code);
    }
   
}
