package subscriptions;

public interface Registration {

    public String getSubscriptionCode();

    public void setSubscriptionCode(final String subscriptionCode);

    public String getBuyerCode();

    public void setBuyerCode(final String buyerCode);
    
    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();

}
