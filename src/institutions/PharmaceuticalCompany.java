package institutions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class PharmaceuticalCompany extends InstitutionAbstract implements Institution {
    
    public PharmaceuticalCompany(final String telephone, final String city, final String street, final String name) {
        super(telephone, city, street, name);
    }

    @Override
    public void update() {
        try {
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(getTelephone().isEmpty() || getCity().isEmpty() || 
                    getStreet().isEmpty() || getName().isEmpty()) {
                throw new SQLException();
            }
            
            if(present) {
                update = db.prepareStatement("UPDATE CASA_FARMACEUTICA SET ViaCasaFarm = ?, "
                        + "CittaCasaFarm = ?, TelefonoCasaFarm = ? WHERE NomeCasaFarmaceutica = ?"); 
                update.setString(1,this.getStreet());
                update.setString(2,this.getCity());
                update.setString(3,this.getTelephone()); 
                update.setString(4,getName()); 
            }
            else {
                update = db.prepareStatement("INSERT INTO CASA_FARMACEUTICA(NomeCasaFarmaceutica,ViaCasaFarm,"
                        + "CittaCasaFarm,TelefonoCasaFarm)"
                        + " VALUE(?,?,?,?)"); 
                update.setString(1,getName()); 
                update.setString(2,this.getStreet());
                update.setString(3,this.getCity());
                update.setString(4,this.getTelephone()); 
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("NomeCasaFarmaceutica", "CASA_FARMACEUTICA", getName());
    }

    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("NomeCasaFarmaceutica", "CASA_FARMACEUTICA", getName());
    }
    
}
