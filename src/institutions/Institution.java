package institutions;

public interface Institution {
    
    public String getTelephone();
    
    public String getCity();
    
    public String getStreet(); 
    
    public String getName();
    
    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();

}
