package institutions;

public abstract class InstitutionAbstract implements Institution {

    private String telephone;
    private String city;
    private String street;
    private String name;

    public InstitutionAbstract(final String telephone, final String city, final String street, final String name) {
        this.city = city;
        this.name = name;
        this.street = street;
        this.telephone = telephone;
    }
    
    @Override
    public String getTelephone() {
        return telephone;
    }

    @Override
    public String getCity() {
        return city;
    }

    @Override
    public String getStreet() {
        return street;
    }
    
    @Override
    public String getName() {
        return name;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
