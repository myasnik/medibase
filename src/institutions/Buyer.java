package institutions;

import java.sql.*;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class Buyer extends InstitutionAbstract implements Institution {
    
    private String code;
    private BuyerType type;
    
    public Buyer(final String code, final BuyerType type, final String telephone, final String city, 
            final String street, final String name) {
        super(telephone, city, street, name);
        this.code = code;
        this.type = type;
    }
    
    public String getCode() {
        return code;
    }

    public void setType(final BuyerType type) {
        this.type = type;
    }

    public BuyerType getType() {
        return this.type;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    @Override
    public void update() {
        try {
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(this.code.isEmpty() || getTelephone().isEmpty() || getCity().isEmpty() || 
                    getStreet().isEmpty() || getName().isEmpty()) {
                throw new SQLException();
            }
            
            if(present) {
                update = db.prepareStatement("UPDATE ACQUIRENTE SET ViaAcquirente = ?, CittaAcquirente = ?,"
                        + " NomeAcquirente = ?, TelefonoAcquirente = ? WHERE CodiceAcquirente = ?"); 
                update.setString(1,this.getStreet());
                update.setString(2,this.getCity());
                update.setString(3,this.getName());
                update.setString(4,this.getTelephone()); 
                update.setString(5,this.code); 
            }
            else {
                if(getType().equals(BuyerType.OSPEDALE)) {
                    update = db.prepareStatement("INSERT INTO ACQUIRENTE(CodiceAcquirente,ViaAcquirente,"
                            + "CittaAcquirente,NomeAcquirente,TelefonoAcquirente,OSPEDALE)"
                            + " VALUE(?,?,?,?,?,?)"); 
                }
                else {
                    update = db.prepareStatement("INSERT INTO ACQUIRENTE(CodiceAcquirente,ViaAcquirente,"
                            + "CittaAcquirente,NomeAcquirente,TelefonoAcquirente,FARMACIA)"
                            + " VALUE(?,?,?,?,?,?)"); 
                }
                update.setString(1,this.code); 
                update.setString(2,this.getStreet());
                update.setString(3,this.getCity());
                update.setString(4,this.getName());
                update.setString(5,this.getTelephone()); 
                update.setString(6,"s"); 
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("CodiceAcquirente", "ACQUIRENTE", this.code);
    }

    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("CodiceAcquirente", "ACQUIRENTE", this.code);
    }

}
