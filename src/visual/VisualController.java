package visual;

import institutions.Buyer;
import institutions.BuyerType;
import institutions.PharmaceuticalCompany;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import medicines.ActiveSubstanceImpl;
import medicines.ClassImpl;
import medicines.FormatImpl;
import medicines.MedicineImpl;
import medicines.ModeImpl;
import medicines.ReferenceImpl;
import subscriptions.RegistrationImpl;
import subscriptions.SubscriptionImpl;
import tools.*;
import transactions.BatchImpl;
import transactions.SellImpl;
import transactions.TransactionImpl;
import transactions.TransactionType;

public class VisualController {

    @FXML
    private TableView<?> medStockView;
    
    @FXML
    private TableView<?> medAcSubView;

    @FXML
    private TableView<?> medInfoView;

    @FXML
    private TableView<?> medInExpView;

    @FXML
    private TableView<?> medExpView;

    @FXML
    private TableView<?> medFinishingView;

    @FXML
    private TableView<?> medControlledView;
    
    @FXML
    private TableView<?> medLowTemp;

    @FXML
    private TableView<?> movBuyView;

    @FXML
    private TableView<?> movBackView;

    @FXML
    private TableView<?> movSellView;

    @FXML
    private TableView<?> movDelView;

    @FXML
    private TableView<?> subUsersView;

    @FXML
    private TableView<?> subContractView;

    @FXML
    private TableView<?> entPharmView;

    @FXML
    private TableView<?> entHospView;

    @FXML
    private TableView<?> entPharmComp;
    
    @FXML
    private TableView<?> classView;
    
    @FXML
    private TableView<?> formatView;
    
    @FXML
    private TableView<?> modView;
    
    @FXML
    private TextField nPhComp;

    @FXML
    private TextField cPhComp;

    @FXML
    private TextField vPhComp;

    @FXML
    private TextField tPhComp;
    
    @FXML
    private TextField cHosp;

    @FXML
    private TextField nHosp;

    @FXML
    private TextField ciHosp;

    @FXML
    private TextField vHosp;

    @FXML
    private TextField tHosp;
    
    @FXML
    private TextField cFarm;

    @FXML
    private TextField nFarm;

    @FXML
    private TextField ciFarm;

    @FXML
    private TextField vFarm;

    @FXML
    private TextField tFarm;

    @FXML
    private TextField nBuy;

    @FXML
    private TextField nSub;

    @FXML
    private TextField nMin;

    @FXML
    private TextField basis;

    @FXML
    private TextField nQuan;

    @FXML
    private TextField nDateStart;

    @FXML
    private TextField nDateEnd;
    
    @FXML
    private TextField nClass;

    @FXML
    private TextField nAcSub;

    @FXML
    private CheckBox traspContCheck;

    @FXML
    private TextField nCount;

    @FXML
    private TextField nUnd;

    @FXML
    private CheckBox lowTempCheck;

    @FXML
    private TextField nChilPos;

    @FXML
    private TextField nAdultPos;

    @FXML
    private TextField nClassNamePrin;
    
    @FXML
    private TextField nMed;

    @FXML
    private TextField nInd;

    @FXML
    private TextField nPharmMed;

    @FXML
    private TextField nActSubMed;
    
    @FXML
    private TextField nModMed;
    
    @FXML
    private TextField nFormMed;

    @FXML
    private TextField nModMedFor;
    
    @FXML
    private TextField nCodMinRef;

    @FXML
    private TextField nMedRef;

    @FXML
    private TextField nMinQuanRef;

    @FXML
    private TextField nFormRef;
    
    @FXML
    private ChoiceBox<TransactionType> tChoiceBox;
    
    @FXML
    private TextField vCodLotto;

    @FXML
    private TextField vNCopy;

    @FXML
    private TextField VExpDate;

    @FXML
    private TextField vNInvoice;

    @FXML
    private TextField vCodMin;

    @FXML
    private TextField vDate;

    @FXML
    private TextField vTime;

    @FXML
    private TextField vCodBuyer;
    
    @FXML
    private TextField delForm;

    @FXML
    private TextField delRef;

    @FXML
    private TextField delMed;

    @FXML
    private TextField delPharmComp;

    @FXML
    private TextField delAcSub;

    @FXML
    private TextField delClass;

    @FXML
    private TextField delBuyer;
    
    @FXML
    private TextField delMod;
    
    @FXML
    private TextField delSub;

    @FXML
    private TextField delBuyerSub;
    
    @FXML
    void exDelSub(ActionEvent event) {
        Delete.deleteMultipleKeys("CodiceAbbonamento", "CodiceAcquirente", "ISCRIZIONE", 
                delSub.getText(), delBuyerSub.getText());
        Delete.deleteSingleKey("CodiceAbbonamento","ABBONAMENTO",delSub.getText());
        delSub.clear();
        delBuyerSub.clear();
    }

    @FXML
    void exDelAcSub(ActionEvent event) {
        Delete.deleteSingleKey("NomePrincipioAttivo", "PRINCIPIO_ATTIVO", delAcSub.getText());
        delAcSub.clear();
    }

    @FXML
    void exDelBuyer(ActionEvent event) {
        Delete.deleteSingleKey("CodiceAcquirente", "ACQUIRENTE", delBuyer.getText());
        delBuyer.clear();
    }

    @FXML
    void exDelClass(ActionEvent event) {
        Delete.deleteSingleKey("NomeClasse", "CLASSE", delClass.getText());
        delClass.clear();
    }

    @FXML
    void exDelForm(ActionEvent event) {
        Delete.deleteSingleKey("NomeFormato", "FORMATO", delForm.getText());
        delForm.clear();
    }

    @FXML
    void exDelMed(ActionEvent event) {
        Delete.deleteSingleKey("NomeCommerciale", "MEDICINALE", delMed.getText());
        delMed.clear();
    }

    @FXML
    void exDelMod(ActionEvent event) {
        Delete.deleteSingleKey("NomeModalita", "MODALITA", delMod.getText());
        delMod.clear();
    }

    @FXML
    void exDelPhCom(ActionEvent event) {
        Delete.deleteSingleKey("NomeCasaFarmaceutica", "CASA_FARMACEUTICA", delPharmComp.getText());
        delPharmComp.clear();
    }

    @FXML
    void exDelRef(ActionEvent event) {
        Delete.deleteSingleKey("CodiceMinisteriale", "REFERENZA", delRef.getText());
        delRef.clear();
    }

    
    @FXML
    void inBuyMedSell(ActionEvent event) {
        if(!Search.isKeyPresent("NumeroFattura","TRANSAZIONE",vNInvoice.getText()) && 
                !Search.isKeyPresent("CodiceLotto", "LOTTO", vCodLotto.getText()) && 
                Search.isKeyPresent("CodiceMinisteriale", "REFERENZA", vCodMin.getText()) &&
                !Search.isKeyPresent("NumeroFattura", "VENDITA", vNInvoice.getText()) &&
                        Search.isKeyPresent("CodiceAcquirente", "ACQUIRENTE", vCodBuyer.getText()) &&
                        General.isTransitionPossible(vCodMin.getText(), vNCopy.getText(), tChoiceBox.getValue())) {
            TransactionImpl in1 = new TransactionImpl(vNInvoice.getText(), vDate.getText(), 
                    vTime.getText(), tChoiceBox.getValue());
            in1.update();
            SellImpl in2 = new SellImpl(vNInvoice.getText(), vCodBuyer.getText());
            in2.update();
            BatchImpl in3 = new BatchImpl(vCodLotto.getText(), vNCopy.getText(), VExpDate.getText()
                    , vNInvoice.getText(), vCodMin.getText());
            in3.update();
        }
        else {
            AlertMessage.alert();
        }
        tChoiceBox.getSelectionModel().selectFirst();
        vCodLotto.clear();
        vNCopy.clear();
        VExpDate.clear();
        vNInvoice.clear();
        vCodMin.clear();
        vDate.clear();
        vTime.clear();
        vCodBuyer.clear();
    }
    
    @FXML
    void inRefRef(ActionEvent event) {
        ReferenceImpl in = new ReferenceImpl(nCodMinRef.getText(), nMinQuanRef.getText(), "0", 
                nMedRef.getText(), nFormRef.getText());
        in.update();
        nCodMinRef.clear();
        nMedRef.clear();
        nMinQuanRef.clear();
        nFormRef.clear();
    }
    
    @FXML
    void inFormMed(ActionEvent event) {
        FormatImpl in = new FormatImpl(nFormMed.getText(), nModMedFor.getText());
        in.update();
        nFormMed.clear();
        nModMedFor.clear();
    }
    
    @FXML
    void inModMed(ActionEvent event) {
        ModeImpl in = new ModeImpl(nModMed.getText());
        in.update();
        nModMed.clear();
    }
    
    @FXML
    void inMed(ActionEvent event) {
        MedicineImpl in = new MedicineImpl(nMed.getText(), nInd.getText(), nPharmMed.getText(), nActSubMed.getText());
        in.update();
        nMed.clear();
        nInd.clear();
        nPharmMed.clear();
        nActSubMed.clear();
    }
    
    @FXML
    void inAcSub(ActionEvent event) {
        ActiveSubstanceImpl in = new ActiveSubstanceImpl(nAcSub.getText(), traspContCheck.isSelected(), nCount.getText()
                , nUnd.getText(), lowTempCheck.isSelected(), nChilPos.getText(), nAdultPos.getText(), nClassNamePrin.getText());
        in.update();
        nAcSub.clear();
        traspContCheck.setSelected(false);
        nCount.clear();
        nUnd.clear();
        lowTempCheck.setSelected(false);
        nChilPos.clear();
        nAdultPos.clear();
        nClassNamePrin.clear();
    }

    @FXML
    void inClass(ActionEvent event) {
        ClassImpl in = new ClassImpl(nClass.getText());
        in.update();
        nClass.clear();
    }

    @FXML
    void inSub(ActionEvent event) {
        if(Search.isKeyPresent("CodiceAcquirente","ACQUIRENTE",nBuy.getText()) && 
                Search.isKeyPresent("CodiceMinisteriale", "REFERENZA", nMin.getText())) {
            SubscriptionImpl in1 = new SubscriptionImpl(nSub.getText(), nQuan.getText(), basis.getText(), 
                    nDateStart.getText(), nDateEnd.getText(), nMin.getText());
            in1.update();
            RegistrationImpl in2 = new RegistrationImpl(nSub.getText(), nBuy.getText());
            in2.update();
        }
        else {
            AlertMessage.alert();
        }
        nSub.clear();
        nQuan.clear();
        basis.clear();
        nDateStart.clear();
        nDateEnd.clear();
        nMin.clear();
        nBuy.clear();
    }

    @FXML
    void inFarm(ActionEvent event) {
        Buyer in = new Buyer(cHosp.getText(), BuyerType.FARMACIA, tHosp.getText(), 
                ciHosp.getText(), vHosp.getText(), nHosp.getText());
        in.update();
        cFarm.clear();
        tFarm.clear();
        ciFarm.clear();
        vFarm.clear();
        nFarm.clear();
    }

    @FXML
    void inHosp(ActionEvent event) {
        Buyer in = new Buyer(cHosp.getText(), BuyerType.OSPEDALE, tHosp.getText(), 
                ciHosp.getText(), vHosp.getText(), nHosp.getText());
        in.update();
        cHosp.clear();
        tHosp.clear();
        ciHosp.clear();
        vHosp.clear();
        nHosp.clear();
    }

    @FXML
    void inPhComp(ActionEvent event) {
        PharmaceuticalCompany in = new PharmaceuticalCompany(tPhComp.getText(), cPhComp.getText(), 
                vPhComp.getText(), nPhComp.getText());
        in.update();
        tPhComp.clear();
        cPhComp.clear();
        vPhComp.clear();
        nPhComp.clear();
    }
    
    @FXML
    void refreshAll(KeyEvent event) {
        if(!event.getCode().equals(KeyCode.ENTER)) {
            return;
        }
        
        FillTableView.fill("SELECT * FROM MODALITA", modView);
        
        FillTableView.fill("SELECT * FROM FORMATO", formatView);
        
        FillTableView.fill("SELECT * FROM CLASSE", classView);
        
        FillTableView.fill("SELECT * FROM PRINCIPIO_ATTIVO", medAcSubView);
        
        FillTableView.fill("SELECT CodiceAcquirente, NomeAcquirente, "
                + "CittaAcquirente, ViaAcquirente, TelefonoAcquirente FROM ACQUIRENTE A "
                + "WHERE A.OSPEDALE IS NOT NULL", entHospView);
        
        FillTableView.fill("SELECT CodiceAcquirente, NomeAcquirente, "
                + "CittaAcquirente, ViaAcquirente, TelefonoAcquirente FROM ACQUIRENTE A "
                + "WHERE A.FARMACIA IS NOT NULL", entPharmView);
        
        FillTableView.fill("SELECT NomeCasaFarmaceutica, CittaCasaFarm, "
                + "ViaCasaFarm, TelefonoCasaFarm FROM CASA_FARMACEUTICA", entPharmComp);
        
        FillTableView.fill("SELECT A.CodiceAbbonamento, A.CodiceMinisteriale, R.NomeCommerciale, R.NomeFormato,"
                + " A.Quantita, A.Cadenza, A.DataInizio, A.DataFine, A2.CodiceAcquirente, "
                + "A2.NomeAcquirente FROM ABBONAMENTO A JOIN ISCRIZIONE I ON A.CodiceAbbonamento = I.CodiceAbbonamento "
                + "JOIN ACQUIRENTE A2 ON I.CodiceAcquirente = A2.CodiceAcquirente "
                + "JOIN REFERENZA R ON A.CodiceMinisteriale = R.CodiceMinisteriale", subContractView);
        
        FillTableView.fill("SELECT ACQUIRENTE.CodiceAcquirente, ACQUIRENTE.NomeAcquirente, "
                + "ACQUIRENTE.CittaAcquirente, ACQUIRENTE.ViaAcquirente, ACQUIRENTE.TelefonoAcquirente "
                + "FROM ACQUIRENTE JOIN ISCRIZIONE ON "
                + "ACQUIRENTE.CodiceAcquirente = ISCRIZIONE.CodiceAcquirente", subUsersView);
        
        FillTableView.fill("SELECT R.CodiceMinisteriale, R.NomeCommerciale, R.NomeFormato, "
                + "L.CodiceLotto, L.NumeroCopie, L.DataScadenza "
                + "FROM LOTTO L JOIN REFERENZA R ON L.CodiceMinisteriale = R.CodiceMinisteriale "
                + "WHERE L.CodiceLotto IN (SELECT L.CodiceLotto FROM LOTTO L "
                + "JOIN TRANSAZIONE T ON L.NumeroFattura = T.NumeroFattura WHERE T.ACQUISTO IS NOT NULL) "
                + "AND DATE(L.DataScadenza) < DATE(NOW())", medExpView);
        
        FillTableView.fill("SELECT R.CodiceMinisteriale, R.NomeCommerciale, R.NomeFormato, "
                + "L.CodiceLotto, L.NumeroCopie, L.DataScadenza "
                + "FROM LOTTO L JOIN REFERENZA R ON L.CodiceMinisteriale = R.CodiceMinisteriale "
                + "WHERE L.CodiceLotto IN (SELECT L.CodiceLotto FROM LOTTO L "
                + "JOIN TRANSAZIONE T ON L.NumeroFattura = T.NumeroFattura WHERE T.ACQUISTO IS NOT NULL) "
                + "AND DATE(L.DataScadenza) > DATE(NOW()) "
                + "AND DATE(L.DataScadenza) < DATE_ADD(NOW(), INTERVAL 30 DAY)", medInExpView);
        
        FillTableView.fill("SELECT CodiceMinisteriale, NomeCommerciale, NomeFormato, Quantita, "
                + "QuantitaMinima FROM REFERENZA WHERE Quantita < QuantitaMinima", medFinishingView);
        
        FillTableView.fill("SELECT R.CodiceMinisteriale, M.NomeCommerciale, R.NomeFormato, P.NomePrincipioAttivo "
                + "FROM REFERENZA R JOIN MEDICINALE M ON M.NomeCommerciale = R.NomeCommerciale "
                + "JOIN PRINCIPIO_ATTIVO P ON M.NomePrincipioAttivo = P.NomePrincipioAttivo "
                + "WHERE P.PotenzialmenteDannoso = \"s\"  ", medControlledView);
        
        FillTableView.fill("SELECT R.CodiceMinisteriale, M.NomeCommerciale, R.NomeFormato, P.NomePrincipioAttivo "
                + "FROM REFERENZA R JOIN MEDICINALE M ON M.NomeCommerciale = R.NomeCommerciale "
                + "JOIN PRINCIPIO_ATTIVO P ON M.NomePrincipioAttivo = P.NomePrincipioAttivo "
                + "WHERE P.ConservazioneBasseTemperature = \"s\"  ", medLowTemp);
        
        FillTableView.fill("SELECT CodiceMinisteriale, NomeCommerciale, NomeFormato, Quantita FROM REFERENZA", medStockView);
        
        FillTableView.fill("SELECT M.NomeCommerciale, F.NomeFormato, F.NomeModalita, P.NomePrincipioAttivo, "
                + "P.NomeClasse, M.NomeCasaFarmaceutica, M.Indicazioni, P.Controindicazioni, "
                + "P.EffettiIndesiderati, P.PosologiaBambini, P.PosologiaAdulti "
                + "FROM FORMATO F JOIN REFERENZA R ON F.NomeFormato = R.NomeFormato "
                + "JOIN MEDICINALE M ON R.NomeCommerciale = M.NomeCommerciale "
                + "JOIN PRINCIPIO_ATTIVO P ON P.NomePrincipioAttivo = M.NomePrincipioAttivo", medInfoView);
        
        FillTableView.fill("SELECT T.NumeroFattura, T.Data, T.Ora, L.CodiceLotto, L.DataScadenza, L.NumeroCopie, "
                + "L.CodiceMinisteriale, M.NomeCommerciale, R.NomeFormato, M.NomeCasaFarmaceutica "
                + "FROM TRANSAZIONE T JOIN LOTTO L ON T.NumeroFattura = L.NumeroFattura "
                + "JOIN REFERENZA R ON R.CodiceMinisteriale = L.CodiceMinisteriale "
                + "JOIN MEDICINALE M ON M.NomeCommerciale = R.NomeCommerciale WHERE ACQUISTO IS NOT NULL", movBuyView);
        
        FillTableView.fill("SELECT T.NumeroFattura, T.Data, T.Ora, L.CodiceLotto, L.NumeroCopie, "
                + "L.CodiceMinisteriale, R.NomeCommerciale, R.NomeFormato, M.NomeCasaFarmaceutica "
                + "FROM TRANSAZIONE T JOIN LOTTO L ON T.NumeroFattura = L.NumeroFattura "
                + "JOIN REFERENZA R ON R.CodiceMinisteriale = L.CodiceMinisteriale "
                + "JOIN MEDICINALE M ON M.NomeCommerciale = R.NomeCommerciale WHERE RESO IS NOT NULL", movBackView);
        
        FillTableView.fill("SELECT T.NumeroFattura, T.Data, T.Ora, L.CodiceLotto, L.NumeroCopie, "
                + "L.CodiceMinisteriale, R.NomeCommerciale, R.NomeFormato "
                + "FROM TRANSAZIONE T JOIN LOTTO L ON T.NumeroFattura = L.NumeroFattura "
                + "JOIN REFERENZA R ON R.CodiceMinisteriale = L.CodiceMinisteriale "
                + "WHERE SMALTIMENTO IS NOT NULL", movDelView);
        
        FillTableView.fill("SELECT T.NumeroFattura, T.Data, T.Ora, L.CodiceLotto, L.NumeroCopie, "
                + "L.CodiceMinisteriale, R.NomeCommerciale, R.NomeFormato, A.CodiceAcquirente, "
                + "A.NomeAcquirente FROM TRANSAZIONE T JOIN LOTTO L ON T.NumeroFattura = L.NumeroFattura "
                + "JOIN REFERENZA R ON R.CodiceMinisteriale = L.CodiceMinisteriale "
                + "JOIN VENDITA V ON V.NumeroFattura = T.NumeroFattura "
                + "JOIN ACQUIRENTE A ON A.CodiceAcquirente = V.CodiceAcquirente WHERE VENDITA IS NOT NULL", movSellView);
    }
    
    public void initialize() {
        tChoiceBox.getItems().setAll(TransactionType.values());
        tChoiceBox.getSelectionModel().selectFirst();
    }
}
