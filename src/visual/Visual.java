package visual;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Visual extends Application{

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        AnchorPane mainPane = (AnchorPane)FXMLLoader.load(Visual.class.getResource("MainWindow.fxml"));
        primaryStage.setTitle("MediBase [BETA]");
        Scene scene = new Scene(mainPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
