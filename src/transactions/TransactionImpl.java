package transactions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class TransactionImpl implements Transaction {

    private String invoiceNumber;
    private String date;
    private String time;
    private TransactionType type;
    
    public TransactionImpl(final String invoiceNumber, final String date, final String time, final TransactionType type) {
        this.invoiceNumber = invoiceNumber;
        this.date = date;
        this.time = time;
        this.type = type;
    }

    @Override
    public String getInvoiceNumber() {
        return invoiceNumber;
    }
    
    @Override
    public void setInvoiceNumber(final String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @Override
    public TransactionType getType() {
        return type;
    }

    @Override
    public void setType(final TransactionType type) {
        this.type = type;
    }

    @Override
    public String getTime() {
        return time;
    }

    @Override
    public void setTime(final String time) {
        this.time = time;
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public void setDate(final String date) {
        this.date = date;
    }

    @Override
    public void update() {
        try {
            if(this.invoiceNumber.isEmpty() || this.date.isEmpty() || this.time.isEmpty()) {
                throw new SQLException();
            }
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(present) {
                update = db.prepareStatement("UPDATE TRANSAZIONE SET Data = ?, Ora = ?"
                        + " WHERE NumeroFattura = ?"); 
                update.setString(1,this.date);
                update.setString(2,this.time);
                update.setString(3,this.invoiceNumber);
            }
            else {
                switch (type) {
                case RESO:
                    update = db.prepareStatement("INSERT INTO TRANSAZIONE(NumeroFattura,Data,"
                            + "Ora,RESO)"
                            + " VALUE(?,?,?,?)"); 
                    break;
                case SMALTIMENTO:
                    update = db.prepareStatement("INSERT INTO TRANSAZIONE(NumeroFattura,Data,"
                            + "Ora,SMALTIMENTO)"
                            + " VALUE(?,?,?,?)");
                    break;
                case ACQUISTO: 
                    update = db.prepareStatement("INSERT INTO TRANSAZIONE(NumeroFattura,Data,"
                            + "Ora,ACQUISTO)"
                            + " VALUE(?,?,?,?)");
                    break;
                case VENDITA:
                    update = db.prepareStatement("INSERT INTO TRANSAZIONE(NumeroFattura,Data,"
                            + "Ora,VENDITA)"
                            + " VALUE(?,?,?,?)");
                    break;
                default:
                    throw new EnumConstantNotPresentException(TransactionType.class, type.toString());
                }
                update.setString(1,this.invoiceNumber); 
                update.setString(2,this.date);
                update.setString(3,this.time);
                update.setString(4, "s");
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException | EnumConstantNotPresentException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("NumeroFattura", "TRANSAZIONE", this.invoiceNumber);
    }

    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("NumeroFattura", "TRANSAZIONE", this.invoiceNumber);
    } 
    
}
