package transactions;

public interface Batch {

    public String getCode();

    public void setCode(final String code);

    public String getQuantity();

    public void setQuantity(final String quantity);

    public String getExpiry();

    public void setExpiry(final String expiry);

    public String getInvoiceNumber();

    public void setInvoiceNumber(final String invoiceNumber);

    public String getMinisterialCode();

    public void setMinisterialCode(final String ministerialCode);
    
    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();

}
