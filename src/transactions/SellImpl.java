package transactions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class SellImpl implements Sell {

    private String invoiceNumber;
    private String buyerCode;
    
    public SellImpl(final String invoiceNumber, final String buyerCode) {
        this.invoiceNumber = invoiceNumber;
        this.buyerCode = buyerCode;
    }

    @Override
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    @Override
    public void setInvoiceNumber(final String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @Override
    public String getBuyerCode() {
        return buyerCode;
    }

    @Override
    public void setBuyerCode(final String buyerCode) {
        this.buyerCode = buyerCode;
    }

    @Override
    public void update() {
        try {
            if(this.invoiceNumber.isEmpty() || this.buyerCode.isEmpty()) {
                throw new SQLException();
            }
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(present) {
                update = db.prepareStatement("UPDATE VENDITA SET CodiceAcquirente = ? "
                        + "WHERE NumeroFattura = ?"); 
                update.setString(1,this.buyerCode);
                update.setString(2,this.invoiceNumber);
            }
            else {
                update = db.prepareStatement("INSERT INTO VENDITA(NumeroFattura,CodiceAcquirente)"
                        + " VALUE(?,?)"); 
                update.setString(1,invoiceNumber); 
                update.setString(2,this.buyerCode);
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("NumeroFattura", "VENDITA", this.invoiceNumber);
    }

    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("NumeroFattura", "VENDITA", this.invoiceNumber);
    }
    
    
}
