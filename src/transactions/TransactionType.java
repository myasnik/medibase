package transactions;

public enum TransactionType {
    VENDITA,
    SMALTIMENTO,
    RESO,
    ACQUISTO
}
