package transactions;

public interface Sell {

    public String getInvoiceNumber();

    public void setInvoiceNumber(final String invoiceNumber);

    public String getBuyerCode();

    public void setBuyerCode(final String buyerCode);
    
    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();
}
