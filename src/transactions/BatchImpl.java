package transactions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class BatchImpl implements Batch {
    
    private String code;
    private String quantity;
    private String expiry;
    private String invoiceNumber;
    private String ministerialCode;
    
    public BatchImpl(final String code, final String quantity, final String expiry, final String invoiceNumber, final String ministerialCode) {
        this.code = code;
        this.quantity = quantity;
        this.expiry = expiry;
        this.invoiceNumber = invoiceNumber;
        this.ministerialCode = ministerialCode;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(final String code) {
        this.code = code;
    }

    @Override
    public String getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(final String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String getExpiry() {
        return expiry;
    }

    @Override
    public void setExpiry(final String expiry) {
        this.expiry = expiry;
    }

    @Override
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    @Override
    public void setInvoiceNumber(final String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @Override
    public String getMinisterialCode() {
        return ministerialCode;
    }

    @Override
    public void setMinisterialCode(final String ministerialCode) {
        this.ministerialCode = ministerialCode;
    }

    @Override
    public void update() {
        try {
            if(this.code.isEmpty() || this.expiry.isEmpty() || this.invoiceNumber.isEmpty()
                    || this.ministerialCode.isEmpty() || this.quantity.isEmpty()) {
                throw new SQLException();
            }
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(present) {
                update = db.prepareStatement("UPDATE LOTTO SET NumeroCopie = ?, DataScadenza = ?,"
                        + " NumeroFattura = ?, CodiceMinisteriale = ? WHERE CodiceLotto = ?"); 
                update.setInt(1,Integer.parseInt(this.quantity));
                update.setString(2,this.expiry);
                update.setString(3,this.invoiceNumber);
                update.setString(4,this.ministerialCode); 
                update.setString(5,this.code); 
            }
            else {
                update = db.prepareStatement("INSERT INTO LOTTO(CodiceLotto, NumeroCopie,"
                        + " DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE(?,?,?,?,?)");
                update.setString(1,this.code);
                update.setInt(2,Integer.parseInt(this.quantity));
                update.setString(3,this.expiry);
                update.setString(4,this.invoiceNumber); 
                update.setString(5,this.ministerialCode); 
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException | NumberFormatException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("CodiceLotto", "LOTTO", this.code);
    }

    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("CodiceLotto", "LOTTO", this.code);
    }
   
}
