package transactions;

public interface Transaction {
    
    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();

    public String getInvoiceNumber();

    public void setInvoiceNumber(final String invoiceNumber);

    public TransactionType getType();

    public void setType(final TransactionType type);

    public String getTime();

    public void setTime(final String time);

    public String getDate();

    public void setDate(final String date);

}
