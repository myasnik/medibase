package medicines;

public interface Format {
    
    public String getName();

    public void setName(final String name);

    public String getMode();

    void setMode(final String mode);
    
    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();

}
