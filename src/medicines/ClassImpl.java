package medicines;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class ClassImpl implements Class {
    
    private String name;

    public ClassImpl(final String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public void update() {
        try {      
            if(this.name.isEmpty()) {
                throw new SQLException();
            }
            if(!isPresent()) {
                Connect.connect();
                Connection db = Connect.getConnection();
                PreparedStatement update;
                update = db.prepareStatement("INSERT INTO CLASSE(NomeClasse) VALUE(?)"); 
                update.setString(1,this.name); 
                update.executeUpdate();
                Connect.disconnect();
            }
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("NomeClasse", "CLASSE", this.name);
    }

    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("NomeClasse", "CLASSE", this.name);
    }

}
