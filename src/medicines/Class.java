package medicines;

public interface Class {
    
    public String getName();
    
    public void setName(final String name);
    
    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();
    
}
