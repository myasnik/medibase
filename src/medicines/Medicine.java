package medicines;

public interface Medicine {
    
    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();

    public void setNameActSub(final String nameActSub);

    public String getNameActSub();

    public void setNamePharmComp(final String namePharmComp);

    public String getNamePharmComp();

    public void setIndications(final String indications);

    public String getIndications();

    public void setName(final String name);

    public String getName();

}
