package medicines;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class ReferenceImpl implements Reference {
    
    private String ministerialCode;
    private String minQuantity;
    private String quantity;
    private String commercialName;
    private String formatName;
    
    
    public ReferenceImpl(final String ministerialCode, final String minQuantity, final String quantity, final String commercialName,
            final String formatName) {
        this.ministerialCode = ministerialCode;
        this.minQuantity = minQuantity;
        this.quantity = quantity;
        this.commercialName = commercialName;
        this.formatName = formatName;
    }

    @Override
    public String getMinisterialCode() {
        return ministerialCode;
    }

    @Override
    public void setMinisterialCode(final String ministerialCode) {
        this.ministerialCode = ministerialCode;
    }

    @Override
    public String getMinQuantity() {
        return minQuantity;
    }

    @Override
    public void setMinQuantity(final String minQuantity) {
        this.minQuantity = minQuantity;
    }

    @Override
    public String getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(final String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String getCommercialName() {
        return commercialName;
    }

    @Override
    public void setCommercialName(final String commercialName) {
        this.commercialName = commercialName;
    }

    @Override
    public String getFormatName() {
        return formatName;
    }

    @Override
    public void setFormatName(final String formatName) {
        this.formatName = formatName;
    }

    @Override
    public void update() {
        try {
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(this.ministerialCode.isEmpty() || this.commercialName.isEmpty() || this.formatName.isEmpty()
                    || this.minQuantity.isEmpty() || this.quantity.isEmpty()) {
                throw new SQLException();
            }
            
            if(present) {
                update = db.prepareStatement("UPDATE REFERENZA SET QuantitaMinima = ?, Quantita = ?,"
                        + " NomeCommerciale = ?, NomeFormato = ? WHERE CodiceMinisteriale = ?"); 
                update.setInt(1,Integer.parseInt(this.minQuantity));
                update.setInt(2,Integer.parseInt(this.quantity));
                update.setString(3,this.commercialName);
                update.setString(4,this.formatName); 
                update.setString(5,this.ministerialCode); 
            }
            else {
                update = db.prepareStatement("INSERT INTO REFERENZA(CodiceMinisteriale, QuantitaMinima,"
                        + " Quantita, NomeCommerciale, NomeFormato) VALUE(?,?,?,?,?,?,?)");
                update.setString(1,this.ministerialCode);
                update.setInt(2,Integer.parseInt(this.minQuantity));
                update.setInt(3,Integer.parseInt(this.quantity));
                update.setString(4,this.commercialName); 
                update.setString(5,this.formatName); 
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException | NumberFormatException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("CodiceMinisteriale", "REFERENZA", this.ministerialCode);
    }

    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("CodiceMinisteriale", "REFERENZA", this.ministerialCode);
    }

}
