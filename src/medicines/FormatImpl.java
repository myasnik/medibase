package medicines;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class FormatImpl implements Format {
    
    private String name;
    private String mode;
    
    public FormatImpl(final String name, final String mode) {
        this.name = name;
        this.mode = mode;
    }

    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public void setName(final String name) {
        this.name = name;
    }
    
    @Override
    public String getMode() {
        return mode;
    }
    
    @Override
    public void setMode(final String mode) {
        this.mode = mode;
    }
    
    @Override
    public void update() {
        try {
            if(this.name.isEmpty() || this.mode.isEmpty()) {
                throw new SQLException();
            }
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(present) {
                update = db.prepareStatement("UPDATE FORMATO SET NomeModalita = ? "
                        + "WHERE NomeFormato = ?"); 
                update.setString(1,this.mode);
                update.setString(2,this.name);
            }
            else {
                update = db.prepareStatement("INSERT INTO FORMATO(NomeFormato,NomeModalita)"
                        + " VALUE(?,?)"); 
                update.setString(1,this.name); 
                update.setString(2,this.mode);
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
    }
    
    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("NomeFormato", "FORMATO", this.name);
    }
    
    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("NomeFormato", "FORMATO", this.name);
    }
    
    

}
