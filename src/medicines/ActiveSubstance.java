package medicines;

public interface ActiveSubstance {
    
    public void update();
    
    public Boolean delete();

    public void setName(final String name);

    public String getName();

    public String getContraindications();

    public void setContraindications(final String contraindications);

    public String getUndesirableEffects();

    public void setUndesirableEffects(final String undesirableEffects);

    public String getChildrenPosology();

    public void setChildrenPosology(final String childrenPosology);

    public String getAdultPosology();

    public void setAdultPosology(final String adultPosology);

    public String getClassName();

    public void setClassName(final String className);

    public Boolean isDangerous();

    public void setDangerous(final Boolean dangerous);

    public void setLowTemp(final Boolean lowTemp);

    public Boolean isLowTemp();
    
    public Boolean isPresent();

}
