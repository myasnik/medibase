package medicines;

public interface Reference {

    public void update();
    
    public Boolean delete();
    
    public Boolean isPresent();

    public String getMinisterialCode();

    public void setMinisterialCode(final String ministerialCode);

    public String getMinQuantity();

    public void setMinQuantity(final String minQuantity);

    public String getQuantity();

    public void setQuantity(final String quantity);

    public String getCommercialName();

    public void setCommercialName(final String commercialName);

    public String getFormatName();

    public void setFormatName(final String formatName);
    
}
