package medicines;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class ActiveSubstanceImpl implements ActiveSubstance {
    
    private String name;
    private Boolean dangerous;
    private String contraindications;
    private String undesirableEffects;
    private Boolean lowTemp;
    private String childrenPosology;
    private String adultPosology;
    private String className;
    
    public ActiveSubstanceImpl(final String name, final Boolean dangerous, final String contraindications, final String undesirableEffects,
            final Boolean lowTemp, final String childrenPosology, final String adultPosology, final String className) {
        this.name = name;
        this.dangerous = dangerous;
        this.contraindications = contraindications;
        this.undesirableEffects = undesirableEffects;
        this.lowTemp = lowTemp;
        this.childrenPosology = childrenPosology;
        this.adultPosology = adultPosology;
        this.className = className;
    }

    @Override
    public void setLowTemp(final Boolean lowTemp) {
        this.lowTemp = lowTemp;
    }
    
    @Override
    public Boolean isLowTemp() {
        return this.lowTemp;
    }
    
    @Override
    public void setDangerous(final Boolean dangerous) {
        this.dangerous = dangerous;
    }
    
    @Override
    public Boolean isDangerous() {
        return this.dangerous;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String getContraindications() {
        return contraindications;
    }

    @Override
    public void setContraindications(final String contraindications) {
        this.contraindications = contraindications;
    }

    @Override
    public String getUndesirableEffects() {
        return undesirableEffects;
    }

    @Override
    public void setUndesirableEffects(final String undesirableEffects) {
        this.undesirableEffects = undesirableEffects;
    }

    @Override
    public String getChildrenPosology() {
        return childrenPosology;
    }

    @Override
    public void setChildrenPosology(final String childrenPosology) {
        this.childrenPosology = childrenPosology;
    }

    @Override
    public String getAdultPosology() {
        return adultPosology;
    }
    
    @Override
    public void setAdultPosology(final String adultPosology) {
        this.adultPosology = adultPosology;
    }

    @Override
    public String getClassName() {
        return className;
    }

    @Override
    public void setClassName(final String className) {
        this.className = className;
    }

    @Override
    public void update() {
        try {
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(this.name.isEmpty() || this.className.isEmpty() || this.adultPosology.isEmpty() || this.childrenPosology.isEmpty()
                    || this.undesirableEffects.isEmpty() || this.contraindications.isEmpty()) {
                throw new SQLException();
            }
            
            if(present) {
                update = db.prepareStatement("UPDATE PRINCIPIO_ATTIVO SET PotenzialmenteDannoso = ?, Controindicazioni = ?,"
                        + " EffettiIndesiderati = ?, ConservazioneBasseTemperature = ?,"
                        + " PosologiaBambini = ?, PosologiaAdulti = ?, NomeClasse = ? WHERE NomePrincipioAttivo = ?"); 
                update.setString(1,(this.dangerous ? "s" : "n"));
                update.setString(2,this.contraindications);
                update.setString(3,this.undesirableEffects);
                update.setString(4,(this.lowTemp ? "s" : "n")); 
                update.setString(5,this.childrenPosology); 
                update.setString(6,this.adultPosology); 
                update.setString(7,this.className); 
                update.setString(8,this.name); 
            }
            else {
                update = db.prepareStatement("INSERT INTO PRINCIPIO_ATTIVO(NomePrincipioAttivo, PotenzialmenteDannoso,"
                        + " Controindicazioni, EffettiIndesiderati, ConservazioneBasseTemperature,"
                        + " PosologiaBambini, PosologiaAdulti, NomeClasse) VALUE(?,?,?,?,?,?,?,?)");
                update.setString(1,this.name); 
                update.setString(2,(this.dangerous ? "s" : "n"));
                update.setString(3,this.contraindications);
                update.setString(4,this.undesirableEffects);
                update.setString(5,(this.lowTemp ? "s" : "n")); 
                update.setString(6,this.childrenPosology); 
                update.setString(7,this.adultPosology); 
                update.setString(8,this.className); 
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
    }

    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("NomePrincipioAttivo", "PRINCIPIO_ATTIVO", this.name);
    }

    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("NomePrincipioAttivo", "PRINCIPIO_ATTIVO", this.name);
    }
    
}
