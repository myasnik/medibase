package medicines;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tools.AlertMessage;
import tools.Connect;
import tools.Delete;
import tools.Search;

public class MedicineImpl implements Medicine {

    private String name;
    private String indications;
    private String namePharmComp;
    private String nameActSub;
    
    public MedicineImpl(final String name, final String indications, final String namePharmComp, final String nameActSub) {
        this.name = name;
        this.indications = indications;
        this.namePharmComp = namePharmComp;
        this.nameActSub = nameActSub;
    }
    
    @Override
    public void update() {
        try {
            if(this.name.isEmpty() || this.nameActSub.isEmpty() || this.namePharmComp.isEmpty() || this.indications.isEmpty()) {
                throw new SQLException();
            }
            Boolean present = isPresent();
            Connect.connect();
            Connection db = Connect.getConnection();
            PreparedStatement update;
            
            if(present) {
                update = db.prepareStatement("UPDATE MEDICINALE SET Indicazioni = ?, NomeCasaFarmaceutica = ?,"
                        + " NomePrincipioAttivo = ? WHERE NomeCommerciale = ?"); 
                update.setString(1,this.indications); 
                update.setString(2,this.namePharmComp);
                update.setString(3,this.nameActSub);
                update.setString(4,this.name);
            }
            else {
                update = db.prepareStatement("INSERT INTO MEDICINALE(NomeCommerciale, Indicazioni,"
                        + " NomeCasaFarmaceutica, NomePrincipioAttivo) VALUE(?,?,?,?)");
                update.setString(1,this.name); 
                update.setString(2,this.indications);
                update.setString(3,this.namePharmComp);
                update.setString(4,this.nameActSub);
            }
            
            update.executeUpdate();
            
            Connect.disconnect();
        } catch (SQLException | ClassNotFoundException e) {
            AlertMessage.alert();
        }
    }
    
    @Override
    public Boolean delete() {
        return Delete.deleteSingleKey("NomeCommerciale", "MEDICINALE", this.name);
    }
    
    @Override
    public Boolean isPresent() {
        return Search.isKeyPresent("NomeCommerciale", "MEDICINALE", this.name);
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public void setName(final String name) {
        this.name = name;
    }
    
    @Override
    public String getIndications() {
        return indications;
    }
    
    @Override
    public void setIndications(final String indications) {
        this.indications = indications;
    }
    
    @Override
    public String getNamePharmComp() {
        return namePharmComp;
    }
    
    @Override
    public void setNamePharmComp(final String namePharmComp) {
        this.namePharmComp = namePharmComp;
    }
    
    @Override
    public String getNameActSub() {
        return nameActSub;
    }
    
    @Override
    public void setNameActSub(final String nameActSub) {
        this.nameActSub = nameActSub;
    }
}
