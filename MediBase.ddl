-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.0              
-- * Generator date: Sep 28 2018              
-- * Generation date: Thu Sep  5 19:57:57 2019 
-- * LUN file: /home/myasnik/Documents/UNI/Database/Progetto/MediBase.lun 
-- * Schema: MainDB_copy/SQL-1 
-- ********************************************* 


-- Database Section
-- ________________ 

use medibase;


-- Tables Section
-- _____________ 

create table ABBONAMENTO (
     CodiceAbbonamento char(254) not null,
     Quantita int not null,
     Cadenza char(254) not null,
     DataInizio date not null,
     DataFine date not null,
     CodiceMinisteriale char(254) not null,
     constraint ID_ABBONAMENTO_ID primary key (CodiceAbbonamento));

create table ACQUIRENTE (
     CodiceAcquirente char(254) not null,
     ViaAcquirente char(254) not null,
     CittaAcquirente char(254) not null,
     NomeAcquirente char(254) not null,
     TelefonoAcquirente char(254) not null,
     OSPEDALE char,
     FARMACIA char,
     constraint ID_ACQUIRENTE_ID primary key (CodiceAcquirente));

create table CASA_FARMACEUTICA (
     NomeCasaFarmaceutica char(254) not null,
     TelefonoCasaFarm char(254) not null,
     ViaCasaFarm char(254) not null,
     CittaCasaFarm char(254) not null,
     constraint ID_CASA_FARMACEUTICA_ID primary key (NomeCasaFarmaceutica));

create table CLASSE (
     NomeClasse char(254) not null,
     constraint ID_CLASSE_ID primary key (NomeClasse));

create table FORMATO (
     NomeFormato char(254) not null,
     NomeModalita char(254) not null,
     constraint ID_FORMATO_ID primary key (NomeFormato));

create table LOTTO (
     CodiceLotto char(254) not null,
     NumeroCopie int not null,
     DataScadenza date not null,
     NumeroFattura char(254) not null,
     CodiceMinisteriale char(254) not null,
     constraint ID_LOTTO_ID primary key (CodiceLotto));

create table MEDICINALE (
     NomeCommerciale char(254) not null,
     Indicazioni char(254) not null,
     NomeCasaFarmaceutica char(254) not null,
     NomePrincipioAttivo char(254) not null,
     constraint ID_MEDICINALE_ID primary key (NomeCommerciale));

create table MODALITA (
     NomeModalita char(254) not null,
     constraint ID_MODALITA_ID primary key (NomeModalita));

create table ISCRIZIONE (
     CodiceAbbonamento char(254) not null,
     CodiceAcquirente char(254) not null,
     constraint ID_ISCRIZIONE_ID primary key (CodiceAcquirente, CodiceAbbonamento));

create table PRINCIPIO_ATTIVO (
     NomePrincipioAttivo char(254) not null,
     PotenzialmenteDannoso char not null,
     Controindicazioni char(100) not null,
     EffettiIndesiderati char(100) not null,
     ConservazioneBasseTemperature char not null,
     PosologiaBambini char(254) not null,
     PosologiaAdulti char(254) not null,
     NomeClasse char(254) not null,
     constraint ID_PRINCIPIO_ATTIVO_ID primary key (NomePrincipioAttivo));

create table REFERENZA (
     CodiceMinisteriale char(254) not null,
     QuantitaMinima int not null,
     Quantita bigint not null,
     NomeCommerciale char(254) not null,
     NomeFormato char(254) not null,
     constraint ID_REFERENZA_ID primary key (CodiceMinisteriale));

create table TRANSAZIONE (
     NumeroFattura char(254) not null,
     Data date not null,
     Ora char(254) not null,
     VENDITA char,
     SMALTIMENTO char,
     RESO char,
     ACQUISTO char,
     constraint ID_TRANSAZIONE_ID primary key (NumeroFattura));

create table VENDITA (
     NumeroFattura char(254) not null,
     CodiceAcquirente char(254) not null,
     constraint FKTRA_VEN_ID primary key (NumeroFattura));


-- Constraints Section
-- ___________________ 

-- Not implemented
-- alter table ABBONAMENTO add constraint ID_ABBONAMENTO_CHK
--     check(exists(select * from ISCRIZIONE
--                  where ISCRIZIONE.CodiceAbbonamento = CodiceAbbonamento)); 

alter table ABBONAMENTO add constraint FKCOMPRENSIONE_FK
     foreign key (CodiceMinisteriale)
     references REFERENZA (CodiceMinisteriale);

alter table ACQUIRENTE add constraint EXTONE_ACQUIRENTE
     check((FARMACIA is not null and OSPEDALE is null)
           or (FARMACIA is null and OSPEDALE is not null)); 

alter table FORMATO add constraint FKUSO_FK
     foreign key (NomeModalita)
     references MODALITA (NomeModalita);

alter table LOTTO add constraint FKMOVIMENTO_FK
     foreign key (NumeroFattura)
     references TRANSAZIONE (NumeroFattura);

alter table LOTTO add constraint FKAPPARTENENZA_FK
     foreign key (CodiceMinisteriale)
     references REFERENZA (CodiceMinisteriale);

alter table MEDICINALE add constraint FKPRODUZIONE_FK
     foreign key (NomeCasaFarmaceutica)
     references CASA_FARMACEUTICA (NomeCasaFarmaceutica);

alter table MEDICINALE add constraint FKCOMPOSIZIONE_FK
     foreign key (NomePrincipioAttivo)
     references PRINCIPIO_ATTIVO (NomePrincipioAttivo);

alter table ISCRIZIONE add constraint FKISC_ACQ
     foreign key (CodiceAcquirente)
     references ACQUIRENTE (CodiceAcquirente);

alter table ISCRIZIONE add constraint FKISC_ABB_FK
     foreign key (CodiceAbbonamento)
     references ABBONAMENTO (CodiceAbbonamento);

alter table PRINCIPIO_ATTIVO add constraint FKPERTINENZA_FK
     foreign key (NomeClasse)
     references CLASSE (NomeClasse);

alter table REFERENZA add constraint FKPARTE_M_FK
     foreign key (NomeCommerciale)
     references MEDICINALE (NomeCommerciale);

alter table REFERENZA add constraint FKPARTE_F_FK
     foreign key (NomeFormato)
     references FORMATO (NomeFormato);

-- Not implemented
-- alter table TRANSAZIONE add constraint ID_TRANSAZIONE_CHK
--     check(exists(select * from LOTTO
--                  where LOTTO.NumeroFattura = NumeroFattura)); 

alter table TRANSAZIONE add constraint EXTONE_TRANSAZIONE
     check((VENDITA is not null and SMALTIMENTO is null and RESO is null and ACQUISTO is null)
           or (VENDITA is null and SMALTIMENTO is not null and RESO is null and ACQUISTO is null)
           or (VENDITA is null and SMALTIMENTO is null and RESO is not null and ACQUISTO is null)
           or (VENDITA is null and SMALTIMENTO is null and RESO is null and ACQUISTO is not null)); 

alter table VENDITA add constraint FKTRA_VEN_FK
     foreign key (NumeroFattura)
     references TRANSAZIONE (NumeroFattura);

alter table VENDITA add constraint FKPARTECIPAZIONE_FK
     foreign key (CodiceAcquirente)
     references ACQUIRENTE (CodiceAcquirente);


-- Index Section
-- _____________ 

create unique index ID_ABBONAMENTO_IND
     on ABBONAMENTO (CodiceAbbonamento);

create index FKCOMPRENSIONE_IND
     on ABBONAMENTO (CodiceMinisteriale);

create unique index ID_ACQUIRENTE_IND
     on ACQUIRENTE (CodiceAcquirente);

create unique index ID_CASA_FARMACEUTICA_IND
     on CASA_FARMACEUTICA (NomeCasaFarmaceutica);

create unique index ID_CLASSE_IND
     on CLASSE (NomeClasse);

create unique index ID_FORMATO_IND
     on FORMATO (NomeFormato);

create index FKUSO_IND
     on FORMATO (NomeModalita);

create unique index ID_LOTTO_IND
     on LOTTO (CodiceLotto);

create index FKMOVIMENTO_IND
     on LOTTO (NumeroFattura);

create index FKAPPARTENENZA_IND
     on LOTTO (CodiceMinisteriale);

create unique index ID_MEDICINALE_IND
     on MEDICINALE (NomeCommerciale);

create index FKPRODUZIONE_IND
     on MEDICINALE (NomeCasaFarmaceutica);

create index FKCOMPOSIZIONE_IND
     on MEDICINALE (NomePrincipioAttivo);

create unique index ID_MODALITA_IND
     on MODALITA (NomeModalita);

create unique index ID_ISCRIZIONE_IND
     on ISCRIZIONE (CodiceAcquirente, CodiceAbbonamento);

create index FKISC_ABB_IND
     on ISCRIZIONE (CodiceAbbonamento);

create unique index ID_PRINCIPIO_ATTIVO_IND
     on PRINCIPIO_ATTIVO (NomePrincipioAttivo);

create index FKPERTINENZA_IND
     on PRINCIPIO_ATTIVO (NomeClasse);

create unique index ID_REFERENZA_IND
     on REFERENZA (CodiceMinisteriale);

create index FKPARTE_M_IND
     on REFERENZA (NomeCommerciale);

create index FKPARTE_F_IND
     on REFERENZA (NomeFormato);

create unique index ID_TRANSAZIONE_IND
     on TRANSAZIONE (NumeroFattura);

create unique index FKTRA_VEN_IND
     on VENDITA (NumeroFattura);

create index FKPARTECIPAZIONE_IND
     on VENDITA (CodiceAcquirente);



INSERT INTO CASA_FARMACEUTICA(NomeCasaFarmaceutica, TelefonoCasaFarm, ViaCasaFarm,  CittaCasaFarm) VALUE("Aesculapius Farmaceutici S.r.l", "030 353 2013", "Via Giovanni Savini", "Brescia");

INSERT INTO CASA_FARMACEUTICA(NomeCasaFarmaceutica, TelefonoCasaFarm, ViaCasaFarm, CittaCasaFarm) VALUE("Laboratoires Sterop"," +32 2 524 39 66 ", "Via Luch", "Anderlecht");

INSERT INTO CASA_FARMACEUTICA(NomeCasaFarmaceutica, TelefonoCasaFarm, ViaCasaFarm, CittaCasaFarm) VALUE("ALFASIGMA SpA", "051 648 9511" , "Via Salvo Maione", "Bologna");

INSERT INTO CASA_FARMACEUTICA(NomeCasaFarmaceutica, TelefonoCasaFarm, ViaCasaFarm, CittaCasaFarm) VALUE("ELI LILLY Italia SpA", "055 42571", "Via Loreto", "Sesto Fiorentino");

INSERT INTO CASA_FARMACEUTICA(NomeCasaFarmaceutica, TelefonoCasaFarm, ViaCasaFarm, CittaCasaFarm) VALUE("RECKIT BENCKISER H Ita" , "02 844751", "Via Rossi", "Milano");



INSERT INTO CLASSE(NomeClasse) VALUE("Antibiotici");

INSERT INTO CLASSE(NomeClasse) VALUE("Stimolanti cardiaci");

INSERT INTO CLASSE(NomeClasse) VALUE("Corticosteroidi sistemici");

INSERT INTO CLASSE(NomeClasse) VALUE("Antidiabetici");

INSERT INTO CLASSE(NomeClasse) VALUE("Farmaci antiinfiammatori ed antireumatici non steroidei");



INSERT INTO MODALITA(NomeModalita) VALUE("Orale");

INSERT INTO MODALITA(NomeModalita) VALUE("Sottocute");

INSERT INTO MODALITA(NomeModalita) VALUE("Rettale");



INSERT INTO PRINCIPIO_ATTIVO(NomePrincipioAttivo, PotenzialmenteDannoso, Controindicazioni, EffettiIndesiderati, ConservazioneBasseTemperature, PosologiaBambini, PosologiaAdulti, NomeClasse) VALUE("Amoxicillina","n", "Allergia", "Diarrea", "n", "50 mg/Kg/die", "3 gr/die", "Antibiotici");

INSERT INTO PRINCIPIO_ATTIVO(NomePrincipioAttivo, PotenzialmenteDannoso, Controindicazioni, EffettiIndesiderati, ConservazioneBasseTemperature, PosologiaBambini, PosologiaAdulti, NomeClasse) VALUE("Adrenalina", "s", "Aritmie", "Tachicardia", "s", "150 mg IM", "1 mg IM", "Stimolanti cardiaci");

INSERT INTO PRINCIPIO_ATTIVO(NomePrincipioAttivo, PotenzialmenteDannoso, Controindicazioni, EffettiIndesiderati, ConservazioneBasseTemperature, PosologiaBambini, PosologiaAdulti, NomeClasse) VALUE("Betametasone", "n", "Infezioni", "Ipertenaione", "n", "0.2 mg/Kg/die", " 5mg/die", "Corticosteroidi sistemici");

INSERT INTO PRINCIPIO_ATTIVO(NomePrincipioAttivo, PotenzialmenteDannoso, Controindicazioni, EffettiIndesiderati, ConservazioneBasseTemperature, PosologiaBambini, PosologiaAdulti, NomeClasse) VALUE("Insulina","s", "Ipersensibilità al principio", "Ipoglicemia", "n", "0.5 UI/Kg/die", " 1 UI/Kg/die", "Antidiabetici");

INSERT INTO PRINCIPIO_ATTIVO(NomePrincipioAttivo, PotenzialmenteDannoso, Controindicazioni, EffettiIndesiderati, ConservazioneBasseTemperature, PosologiaBambini, PosologiaAdulti, NomeClasse) VALUE("Ibuprofene","n", "Insufficienza Renale", "Emorragia gastrica", "n", "10 mg/Kg/die", " 1.5 gr/die", "Farmaci antiinfiammatori ed antireumatici non steroidei");



INSERT INTO MEDICINALE(NomeCommerciale, Indicazioni, NomeCasaFarmaceutica, NomePrincipioAttivo) VALUE("AMOXINA", "Infezioni da germi sensibili all’amoxicillina", "Aesculapius Farmaceutici S.r.l", "Amoxicillina"); 

INSERT INTO MEDICINALE(NomeCommerciale, Indicazioni, NomeCasaFarmaceutica, NomePrincipioAttivo) VALUE("LEVORENINE", "Shock anafilattico", "Laboratoires Sterop", "Adrenalina");

INSERT INTO MEDICINALE(NomeCommerciale, Indicazioni, NomeCasaFarmaceutica, NomePrincipioAttivo) VALUE("BENTELAN", "Asma bronchiale, artrite reumatoide", "ALFASIGMA SpA", "Betametasone");

INSERT INTO MEDICINALE(NomeCommerciale, Indicazioni, NomeCasaFarmaceutica, NomePrincipioAttivo) VALUE("HUMALOG", "Diabete mellito", "ELI LILLY Italia SpA", "Insulina");

INSERT INTO MEDICINALE(NomeCommerciale, Indicazioni, NomeCasaFarmaceutica, NomePrincipioAttivo) VALUE("NUROFEN", "Dolori di varia natura", "RECKIT BENCKISER H Ita", "Ibuprofene");



INSERT INTO FORMATO(NomeFormato, NomeModalita) VALUE("Sospensione granulare 5%", "Orale"); 

INSERT INTO FORMATO(NomeFormato, NomeModalita) VALUE("Fiale 1 mg/1ml", "Sottocute"); 

INSERT INTO FORMATO(NomeFormato, NomeModalita) VALUE("Compresse effervescenti 0.5 mg", "Orale"); 

INSERT INTO FORMATO(NomeFormato, NomeModalita) VALUE("Supposte 125 mg", "Rettale"); 

INSERT INTO FORMATO(NomeFormato, NomeModalita) VALUE("Compresse 100 mg", "Orale"); 



INSERT INTO REFERENZA(CodiceMinisteriale, QuantitaMinima, Quantita, NomeCommerciale, NomeFormato) VALUE("023966094","200", "90", "AMOXINA", "Sospensione granulare 5%"); 

INSERT INTO REFERENZA(CodiceMinisteriale, QuantitaMinima, Quantita, NomeCommerciale, NomeFormato) VALUE("032145678", "50", "180", "LEVORENINE", "Fiale 1 mg/1ml");

INSERT INTO REFERENZA(CodiceMinisteriale, QuantitaMinima, Quantita, NomeCommerciale, NomeFormato) VALUE("019655152", "100", "45", "BENTELAN", "Compresse effervescenti 0.5 mg"); 

INSERT INTO REFERENZA(CodiceMinisteriale, QuantitaMinima, Quantita, NomeCommerciale, NomeFormato) VALUE("033637024", "500", "450", "HUMALOG", "Fiale 1 mg/1ml"); 

INSERT INTO REFERENZA(CodiceMinisteriale, QuantitaMinima, Quantita, NomeCommerciale, NomeFormato) VALUE("034102020", "100", "0", "NUROFEN", "Supposte 125 mg");

INSERT INTO REFERENZA(CodiceMinisteriale, QuantitaMinima, Quantita, NomeCommerciale, NomeFormato) VALUE("079102020", "20", "30", "NUROFEN", "Compresse 100 mg");



INSERT INTO ABBONAMENTO(CodiceAbbonamento, Quantita, Cadenza, DataInizio, DataFine, CodiceMinisteriale) VALUE("LAB31", "20", "Ogni martedì","2019-01-01", "2022-12-31", "032145678");

INSERT INTO ABBONAMENTO(CodiceAbbonamento, Quantita, Cadenza, DataInizio, DataFine, CodiceMinisteriale) VALUE("ALF61", "100", "Ogni primo giorno del mese","2018-01-01", "2019-12-31", "019655152");

INSERT INTO ABBONAMENTO(CodiceAbbonamento, Quantita, Cadenza, DataInizio, DataFine, CodiceMinisteriale) VALUE("ELI28", "30", "Ogni due lunedì","2019-01-01", "2020-12-31", "033637024");

INSERT INTO ABBONAMENTO(CodiceAbbonamento, Quantita, Cadenza, DataInizio, DataFine, CodiceMinisteriale) VALUE("REC12", "100", "Settimanale","2019-01-06", "2019-11-29", "034102020");



INSERT INTO ACQUIRENTE(CodiceAcquirente, ViaAcquirente, CittaAcquirente, NomeAcquirente, TelefonoAcquirente, FARMACIA) VALUE("F230", "Piazza G. Mazzini 9", "San Mauro Pascoli","Farmacia Pivi", "0541 932899","s");

INSERT INTO ACQUIRENTE(CodiceAcquirente, ViaAcquirente, CittaAcquirente, NomeAcquirente, TelefonoAcquirente, OSPEDALE) VALUE("HC01", "Viale Giovanni Ghirotti 286", "Cesena","Ospedale Maurizio Bufalini", "0747 352111", "s");

INSERT INTO ACQUIRENTE(CodiceAcquirente, ViaAcquirente, CittaAcquirente, NomeAcquirente, TelefonoAcquirente, FARMACIA) VALUE("F031", "Via Giovanni Pascoli 53", "Mercato Saraceno", "Farmacia del borgo", "0543 703865", "s");

INSERT INTO ACQUIRENTE(CodiceAcquirente, ViaAcquirente, CittaAcquirente, NomeAcquirente, TelefonoAcquirente, OSPEDALE) VALUE("HR06", "Via Luigi settembrini 2", "Rimini", "Ospedale degli infermi", "0541 704343", "s");

INSERT INTO ACQUIRENTE(CodiceAcquirente, ViaAcquirente, CittaAcquirente, NomeAcquirente, TelefonoAcquirente, FARMACIA) VALUE("FF32", "Via Felice Orsini 3", "Predappio", "Farmacia Lusertoli", "0543 393278", "s");



INSERT INTO ISCRIZIONE(CodiceAbbonamento, CodiceAcquirente) VALUE("LAB31", "HC01");

INSERT INTO ISCRIZIONE(CodiceAbbonamento, CodiceAcquirente) VALUE("ALF61", "F031");

INSERT INTO ISCRIZIONE(CodiceAbbonamento, CodiceAcquirente) VALUE("ELI28", "HR06");

INSERT INTO ISCRIZIONE(CodiceAbbonamento, CodiceAcquirente) VALUE("REC12", "FF32");



INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, ACQUISTO) VALUE("195-213","2019-05-13", "11:55", "s");

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, ACQUISTO) VALUE( "209-746","2019-02-03", "10:25", "s"); 

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, ACQUISTO) VALUE( "420-901","2019-04-11", "09:15", "s");

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, ACQUISTO) VALUE( "198-236","2019-01-21", "11:25", "s");

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, ACQUISTO) VALUE( "902-454","2017-01-13", "08:05", "s");

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, VENDITA) VALUE("219-746","2019-05-01", "12:51", "s");

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, VENDITA) VALUE( "005-213","2019-06-01", "11:22", "s");

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, VENDITA) VALUE( "450-231","2019-08-02", "09:34", "s");

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, SMALTIMENTO) VALUE( "900-754","2017-11-20", "09:34", "s");

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, RESO) VALUE( "208-216","2019-06-23", "17:12", "s");

INSERT INTO TRANSAZIONE(NumeroFattura, Data, Ora, ACQUISTO) VALUE( "899-754","2018-02-23", "17:02", "s");



INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("A746", "100", "2020-10-07","209-746","023966094");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("L213", "200", "2019-12-15","195-213","032145678");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("B901", "50", "2019-09-29","420-901","019655152");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("H236", "500", "2020-01-08","198-236","033637024");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("N454", "250", "2017-10-27","902-454","034102020");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("B566", "10", "2020-10-07","219-746","023966094");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("A213", "20", "2019-12-15","005-213","032145678");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("U987", "5", "2019-09-29","450-231","019655152");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("P098", "50", "2020-01-08","208-216","033637024");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("Z234", "250", "2017-10-27","900-754","034102020");

INSERT INTO LOTTO(CodiceLotto, NumeroCopie, DataScadenza, NumeroFattura, CodiceMinisteriale) VALUE ("Y224", "30", "2019-09-01","899-754","079102020");



INSERT INTO VENDITA(NumeroFattura, CodiceAcquirente) VALUE("219-746", "F230");

INSERT INTO VENDITA(NumeroFattura, CodiceAcquirente) VALUE("005-213", "HC01");

INSERT INTO VENDITA(NumeroFattura, CodiceAcquirente) VALUE("450-231", "HR06");
